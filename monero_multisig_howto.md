# Monero M/N HOWTO

Intended for use with monero-wallet-cli.

## Create and prepare shared wallet
1. Alice, Bob, and Charlie all create a new empty wallet
2. They all run `prepare_multisig` on their wallets
3. Alice sends the output (her 'Multisig...' key) via secure sidechannels to Bob and Charlie. Bob does the same with his key to Alice and Charlie, Charlie does the same with his key to Alice and Bob
4. They all run `make_multisig` on the other 2's keys: `make_multisig 2 Multisig1... Multisig2...` (we pick 2 for threshold for 2/3 multisig)
5. They all run `exchange_multisig_keys` on the other 2's NEW keys: `exchange_multisig_keys Multisig1... Multisig2...`

## To receive funds

1. Someone sends funds to this new shared wallet
2. Alice, Bob, and Charlie refresh their shared wallet
3. They all run `export_multisig_info` and securely send the file to each other. Example, Alice runs `export_multisig_info ms_1`, Charlie runs `export_multisig_info ms_3`
4. They all run `import_multisig_info` and import the other's respective files. Example, Bob runs `import_multisig_info ms_1 ms_3`
5. They all run `refresh`

Repeat `export_multisig_info` and `import_multisig_info` as needed.

## To send funds

1. Perform a transfer as usual from any of the signers to a monero address
2. An unsigned TX will be written to a file in the directory you started the wallet from, titled `multisig_monero_tx`
3. Send this file to at least 1 of the other 2 signers
4. Any 1 of those 2 people then moves the file into the directory they started the wallet and issues the command `sign_multisig multisig_monero_tx`
5. The person who signed now submits to the network with `submit_multisig multisig_monero_tx`

Repeat `export_multisig_info` and `import_multisig_info` as needed.
