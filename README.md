## Welcome to the Kovri Props & Funds Protocol (KPFP)

Currently, this protocol is a simple git/markdown crowd-funding and proposition process. Let's explore:

### Step 1

- Click on the "Issues" tracker

![Step 1](./png/step_1.png)

### Step 2

- Choose a template. We'll start with the "General Request for Funding"

![Step 2](./png/step_2.png)

### Step 3

- Fill out the form

![Step 3](./png/step_3.png)

### Step 4

- Click "Preview" to review before submitting

![Step 4](./png/step_4.png)

### Step 5

- Scroll to the bottom of the page and click "Submit issue"

![Step 5](./png/step_5.png)

Note: see the completed example request [here](https://gitlab.com/kovri-project/kovri-fund/issues/2)

### Step 6

Request completed! Now, broadcast this request to the world!

- You'll need backers to support your request. Tell friends, co-workers, and other communities about your Kovri 
work!
- The Kovri Project Fund *may* be able to fulfill all or part of your request depending on availability
- If you haven't done so already, [give a shout-out to the Kovri 
community](https://gitlab.com/kovri-project/kovri#contact)

*Note: the Monero Project has a [Forum Funding System](https://forum.getmonero.org/) which you can cross-post to 
so long as your request benefits the Monero Project*

### Step 7

- The community and project team will arbitrate the request
- If the request is deemed suitable, the Kovri Project will authorize the collection of funds into the Kovri 
Project coffers
- All future work and payouts for this request will then be detailed and cataloged within the open repository 
issue
- Payment ID's or equivalent will be posted as proof that funds were sent
- If at any time there is a discrepancy with work or payout, discussions will be had within the open issue
- Milestones **must** be completed in order to receive a payout

